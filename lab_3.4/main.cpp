﻿#include <iostream>
#include <fstream>
#include "DateTime.h"
#include "TimeStr.h"
#include "Event.h"
#include "TimeInterval.h"

int main()
{
    try {
        TimeStr dates[3];
        dates[0] = TimeStr(2023, 1, 1, 12, 15, false);
        dates[1] = TimeStr(dates[0]);
        dates[2] = TimeStr();

        std::cout << "Type the Date 3 (yyyy mm dd hh mm 0-am,1-pm): ";
        std::cin >> dates[2];

        std::cout << "Date 1: " << dates[0] << '\n';
        std::cout << "Date 2: " << dates[1] << " (Date 1 copy)\n";
        std::cout << "Date 3: " << dates[2] << " (Modified)\n";
        std::cout << "-----------------------------------------------\n";

        Event events[3];
        events[0] = Event(2023, 1, 1, 12, 15, "Event_1");
        events[1] = Event(events[0]);
        events[2] = Event();

        std::cout << "Type the Event 3 (yyyy mm dd hh mm event_name): ";
        std::cin >> events[2];

        std::cout << "Event 1: " << events[0] << '\n';
        std::cout << "Event 2: " << events[1] << " (Event 1 copy)\n";
        std::cout << "Event 3: " << events[2] << " (Modified)\n";
        std::cout << "-----------------------------------------------\n";
    }
    catch (const std::string e) {
        std::cout << "[ERROR]: " << e << '\n';
        return -1;
    }

    return 0;
}