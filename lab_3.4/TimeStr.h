#pragma once
#include "DateTime.h"
#include <string>
#include <iostream>
#include <fstream>

class TimeStr :
    public DateTime
{
private:
    std::string timeStr;

public:
    TimeStr();
    TimeStr(int year, int month, int day, int hour, int minute, bool pm);
    TimeStr(TimeStr& time);

    void setTime(int hour, int minute, bool pm);

    std::string toStr();

    friend std::ostream& operator << (std::ostream& os, TimeStr& time);
    friend std::istream& operator >> (std::istream& is, TimeStr& time);
    friend std::fstream& operator << (std::fstream& os, TimeStr& time);
    friend std::fstream& operator >> (std::fstream& is, TimeStr& time);
};

